(* Dat Bae *)

From Coq Require Import
  ExtrOcamlIntConv
  NArith
  List
  Ascii
  String.

From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf Scanf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Inductive tree : Set :=
| Out : list bool -> (list bool -> tree) -> tree
| Guess : list N -> tree
.

Fixpoint bin_succ (xs : list bool) : list bool :=
  match xs with
  | [] => []
  | false :: xs => true :: xs
  | true :: xs => false :: bin_succ xs
  end.

Definition test_matrix (nN nF : N) : list (list bool) :=
  N_repeatf bin_succ (N_repeat false nF) nN.

Fixpoint _analyze {A B} (eqb_ : A -> B -> bool) (i : N) (xs : list A) (ys : list B) : list N :=
  match xs, ys with
  | [], _ => []
  | x :: xs', [] => i :: _analyze eqb_ (N.succ i) xs' []
  | x :: xs', y :: ys' =>
    if eqb_ x y then
      _analyze eqb_ (N.succ i) xs' ys'
    else
      i :: _analyze eqb_ (N.succ i) xs' ys
  end.

Definition analyze : list (list bool) -> list (list bool) -> list N :=
  _analyze (list_eqb Bool.eqb) 0.

Section UNIT.

Local Notation T := true.
Local Notation F := false.

(* Unit test. *)
Example analyze_test
  : analyze
      [[F;F;F]
      ;[T;F;F]
      ;[F;T;F]
      ;[T;T;F]
      ;[F;F;T]
      ]
      [[F;F;F]
      ;[F;F;T]
      ] = [1;2;3]%N.
Proof. reflexivity. Qed.

End UNIT.

Fixpoint _diagnostic (ts : list (list bool)) (k : list (list bool) -> list N) : tree :=
  match ts with
  | [] => Guess (k [])
  | t :: ts => Out t (fun u => _diagnostic ts (fun us => k (u :: us)))
  end.

Definition diagnostic (ts : list (list bool)) : tree :=
  _diagnostic (transpose ts) (fun us => analyze ts (transpose us)).

Definition tester (nN nB nF : N) : tree :=
  diagnostic (test_matrix nN nF).

Definition to_line : list bool -> list char :=
  map (fun b : bool => if b then char_of_ascii "1"%char else char_of_ascii "0"%char).

Definition from_line : list char -> list bool :=
  map (fun c : char => char_eqb "1"%char c).

Fixpoint print_numbers (xs : list N) : IO unit :=
  match xs with
  | [] => IO.ret tt
  | [x] => print_int (int_of_n x) ;; print_newline
  | x :: xs => print_int (int_of_n x) ;; print_string " "%string ;; print_numbers xs
  end.

Fixpoint run_tree (t : tree) : IO unit :=
  match t with
  | Out r k =>
    print_endline (OString.of_list (to_line r)) ;;
    s <- read_line ;;
    run_tree (k (from_line (OString.to_list s)))
  | Guess r =>
    print_numbers r ;;
    i <- read_int ;;
    if int_eqb (int_of_n 1) i then
      IO.ret tt
    else
      exit (int_of_n 1)
  end.

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun i =>
    nbf <- scanf "%Nd %Nd %Nd" (fun n1 n2 n3 _ => Some (n1, n2, n3)) ;;
    let '(nN, nB, nF) := nbf in
    run_tree (tester nN nB nF)).
