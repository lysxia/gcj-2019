(* You Can Go Your Own Way *)

From Coq Require Import
  ExtrOcamlIntConv
  NArith
  ZArith
  List
  Ascii
  String.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Local Open Scope Z_scope.

Inductive dir := E | S.

Definition to_char (d : dir) : char :=
  match d with
  | E => "E"%char
  | S => "S"%char
  end.

Definition from_char (c : char) : dir :=
  if char_eqb "E"%char c then E else S.

Definition move (d : dir) (i : Z) : Z :=
  match d with
  | E => i + 1
  | S => i - 1
  end.

Fixpoint _solve (i : Z) (xs : list dir) : list dir :=
  match xs with
  | [] | _ :: [] => []
  | x :: y :: xs =>
    let i' := move x i in
    let j := move y i' in
    if Z.ltb 0 i' then
      S :: E :: _solve j xs
    else
      E :: S :: _solve j xs
  end.

Definition solve : list dir -> list dir := _solve 0.

Definition solve_o : ocaml_string -> ocaml_string :=
  fun s => OString.of_list (map to_char (solve (map from_char (OString.to_list s)))).

Local Open Scope N_scope.

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun (i : N) =>
    len <- read_N ;;
    s <- read_line ;;
    print_string (sprintf "Case #%Nd: " i) ;;
    print_endline (solve_o s)).
