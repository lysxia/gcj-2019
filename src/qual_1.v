(* Foregone Solution *)

From Coq Require Import
  ExtrOcamlIntConv
  NArith
  List
  Ascii
  String.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Local Infix "::" := String : string_scope.

(* Implement *)

Fixpoint solve' (s : list char) (a1 : list char) (a2 : list char) : list char * list char :=
  match s with
  | [] => (a1, a2)
  | c :: s' =>
    if char_eqb c "4"%char then
      solve' s' (("2"%char : char) :: a1) (("2"%char : char) :: a2)
    else
      solve' s' (c :: a1) (("0"%char : char) :: a2)
  end.

Definition solve : list char -> list char * list char :=
  fun s => solve' (List.rev' s) [] [].

(* Run *)

Definition solve_o : ocaml_string -> ocaml_string * ocaml_string :=
  fun s =>
    let '(s1, s2) := solve (OString.to_list s) in
    (OString.of_list s1, OString.of_list s2).

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun i =>
    s <- read_line ;;
    let '(s1, s2) := solve_o s in
    print_string (sprintf "Case #%Nd: %s %s" i (from_ostring s1) (from_ostring s2));;
    print_newline).

