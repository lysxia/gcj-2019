From SimpleIO Require Import SimpleIO.
From GCJ2019 Require Import qual_3.

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "qual_3.ml" run_main.
