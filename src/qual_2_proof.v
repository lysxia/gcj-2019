From Lib Require Import common.
From GCJ2019 Require Import qual_2.

From Coq Require Import
  List
  ZArith
  Arith
  Lia.
From SimpleIO Require Import SimpleIO.

Import ListNotations.

Local Open Scope Z_scope.

(** Instead of tracking coordinates [(x, y)],
    we use the basis [(x - y, x + y)] ([x] goes right, [y] goes to down).

    [x + y] increases by 1 at every E/S step, so we can keep it implicit.
    [i := x - y] either goes up or down by 1.
  *)

(** [path xs]: list of [i] coordinates covered by path [xs]. *)

Fixpoint _path (i : Z) (xs : list dir) : list Z :=
  i :: match xs with
       | [] => []
       | x :: xs => _path (move x i) xs
       end.

Definition path : list dir -> list Z := _path 0.

(** Starting from positions [i] and [j] (same point in time),
    the two paths never share an edge: anytime they cross, they
    immediately split in different directions. *)
Inductive disjoint_paths (i j : Z) : list dir -> list dir -> Prop :=
| disjoint_nil : disjoint_paths i j [] []
| disjoint_cons x xs y ys
  : (i <> j \/ (i = j /\ x <> y)) ->
    disjoint_paths (move x i) (move y j) xs ys ->
    disjoint_paths i j (x :: xs) (y :: ys)
.

(** A valid path in a maze of size [n] must have length [2 * n]
    and the last coordinate [i := x - y] must be [0]. *)
Definition valid (n : nat) (xs : list dir) : Prop :=
  List.length xs = (2 * n)%nat /\ last (path xs) 1 = 0.

(** Assuming a valid input path [xs]:
    1. our path [solve xs] is valid;
    2. our path [solve xs] does not cross the input path [xs]. *)
Definition CORRECT : Prop :=
  forall n xs,
    valid n xs ->
    valid n (solve xs) /\
    disjoint_paths 0 0 xs (solve xs).

Local Notation succ := Datatypes.S.

Inductive evenlen {A} : nat -> list A -> Prop :=
| evenlen_nil : evenlen 0 []
| evenlen_conscons n x y xs : evenlen n xs -> evenlen (succ n) (x :: y :: xs)
.

Lemma evenlen_spec {A} (n : nat) (xs : list A)
  : List.length xs = (2 * n)%nat -> evenlen n xs.
Proof.
  revert xs; induction n; destruct xs as [ | x xs ]; try discriminate.
  - constructor.
  - rewrite Nat.mul_succ_r, Nat.add_comm.
    destruct xs as [ | y xs ]; try discriminate.
    intros H.
    do 2 apply Nat.succ_inj in H.
    constructor; auto.
Qed.

Lemma evenlen_cospec {A} (n : nat) (xs : list A)
  : evenlen n xs -> List.length xs = (2 * n)%nat.
Proof.
  induction 1.
  - constructor.
  - cbn. rewrite IHevenlen, Nat.add_succ_r. reflexivity.
Qed.

Fixpoint last' {A} (l : list A) (a : A) : A :=
  match l with
  | [] => a
  | x :: l => last' l x
  end.

Lemma last_spec {A} (l : list A)
  : forall (a : A), last l a = last' l a.
Proof.
  induction l as [ | y l IHl ].
  - reflexivity.
  - cbn. intros a. destruct l.
    + reflexivity.
    + rewrite IHl. reflexivity.
Qed.

Lemma _solve_valid
  : forall n xs,
    evenlen n xs ->
    forall i,
    evenlen n (_solve i xs) /\
    forall w, last (_path 0 (_solve i xs)) w = 0%Z.
Proof.
  induction 1.
  - repeat constructor.
  - cbn [ _solve ]; intros i.
    destruct (IHevenlen (move y (move x i))) as [I J].
    destruct (Z.ltb_spec 0 (move x i));
      repeat constructor; auto;
      intros; rewrite last_spec; cbn; rewrite <- last_spec; auto.
Qed.

Lemma solve_valid
  : forall n xs, valid n xs -> valid n (solve xs).
Proof.
  intros n xs [Hlen Hlast].
  edestruct _solve_valid; eauto using evenlen_spec.
  split; eauto using evenlen_cospec.
Qed.

Lemma _solve_disjoint n xs
  : evenlen n xs ->
    forall i,
      Zeven i -> disjoint_paths i 0 xs (_solve i xs).
Proof.
  induction 1; intros i Hi; cbn [ _solve ].
  - constructor.
  - destruct (Z.ltb_spec 0 (move x i));
      destruct x eqn:Ex, y eqn:Ey; cbn in *;
      (constructor; [ | constructor; [ | apply IHevenlen ] ]); cbn.
    all: try lia.
    all: try match goal with
             | [ |- Zeven ?j ] =>
               (replace j with i by lia; auto) +
               (replace j with (i + 2) by lia) +
               replace j with (i + (- 2)) by lia;
               try apply Zeven_plus_Zeven; cbn; auto
             end.
    all: destruct (Z.eq_dec i 0); [ right; split; eauto; discriminate | left; auto ].
Qed.

Lemma solve_disjoint
  : forall n xs,
      valid n xs ->
      disjoint_paths 0 0 xs (solve xs).
Proof.
  intros; eapply _solve_disjoint.
  - apply evenlen_spec, H.
  - constructor.
Qed.

Lemma solve_correct : CORRECT.
Proof.
  split.
  - apply solve_valid; auto.
  - eapply solve_disjoint; eauto.
Qed.
