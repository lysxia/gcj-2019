From Lib Require Import common.
From GCJ2019 Require Import qual_1.

From Coq Require Import
  List
  Arith
  Lia
  Ascii
  String.
From SimpleIO Require Import SimpleIO.

Import ListNotations.

(** * Definition of correctness *)

(** ** Preliminary definitions *)

(* [decimal_string' s n]: [s] is a decimal representation of [n].
   Little endian: the head of the string is the least significant digit.   *)
Inductive decimal_string' : list char -> nat -> Prop :=
| decimal_nil : decimal_string' [] 0
| decimal_cons c d s n : char_as_nat c d -> decimal_string' s n -> decimal_string' (c :: s) (d + 10 * n)
.

(* Big endian: the head of the string is the most significant digit. *)
Definition decimal_string (s : list char) (n : nat) : Prop := decimal_string' (List.rev s) n.

(* Strings which don't contain ["4"]. *)
Definition no4 : list char -> Prop :=
  Forall (fun c : char => c <> "4"%char).

(** ** Main theorem *)

(* If the input [s] is a decimal representation of a number [n],
   then [solve s] produces two strings without the digit ["4"],
   and representing numbers [n1], [n2], which sum to [n]. *)
Definition SOLVE_CORRECT : Prop :=
  forall s s1 s2 n,
    decimal_string s n ->
    (s1, s2) = solve s ->
    no4 s1 /\
    no4 s2 /\
    exists n1 n2,
      decimal_string s1 n1 /\
      decimal_string s2 n2 /\
      n = n1 + n2.

(** * Proof of correctness *)

(** ** Some general facts *)

Lemma decimal_string'_injective s n1
  : decimal_string' s n1 ->
    forall n2,
    decimal_string' s n2 ->
    n1 = n2.
Proof.
  induction 1; inversion 1; subst.
  - reflexivity.
  - match goal with
    | [ IH : _, J : _ |- _ ] => apply IH in J; clear IH
    end.
    match goal with
    | [ H : char_as_nat _ ?x, I : char_as_nat _ ?y |- _ ] => apply (char_as_nat_injective _ _ _ H) in I
    end.
    subst; reflexivity.
Qed.

Lemma decimal_digits :
  forall s n,
    decimal_string s n ->
    List.Forall (fun c => exists n, char_as_nat c n) s.
Proof.
  unfold decimal_string.
  intros s n H.
  rewrite <- (rev_involutive s).
  remember (rev s) eqn:E; clear E.
  apply Forall_rev.
  induction H; eauto.
Qed.

(** ** Relation between the input and outputs of [solve]. *)

Inductive zippy : list char -> list char -> list char -> Prop :=
| zippy_nil : zippy [] [] []
| zippy_cons a b c x y z xs ys zs
  : char_as_nat x a ->
    char_as_nat y b ->
    char_as_nat z c ->
    a = b + c ->
    zippy xs ys zs -> zippy (x :: xs) (y :: ys) (z :: zs)
.

(** [solve_zippy]: [solve] implements the [zippy] relation *)

Lemma _solve'_zippy :
  forall (s z a1 a2 s1 s2 : list char),
    List.Forall (fun c => exists n, char_as_nat c n) s ->
    zippy z a1 a2 ->
    (s1, s2) = solve' s a1 a2 ->
    zippy (List.rev s ++ z) s1 s2.
Proof.
  induction s; cbn; intros.
  - inversion H1. assumption.
  - rewrite <- app_assoc; cbn.
    inversion H; subst.
    destruct (char_eqb a "4"%char) eqn:Ea.
    + apply char_eqb_eq in Ea; subst.
      eapply IHs; [ assumption | | eassumption ].
      econstructor;
        try apply char_as_nat_4;
        try apply char_as_nat_2;
        try reflexivity + assumption.
    + eapply IHs; [ assumption | | eassumption ].
      destruct H4.
      econstructor;
        try eassumption;
        try apply char_as_nat_0.
      apply plus_n_O.
Qed.

Lemma solve_zippy :
  forall s s1 s2,
    List.Forall (fun c => exists n, char_as_nat c n) s ->
    (s1, s2) = solve s ->
    zippy s s1 s2.
Proof.
  intros s s1 s2 H I.
  unfold solve, rev' in I. rewrite <- rev_alt in I.
  eapply _solve'_zippy in I;
    try (apply Forall_rev; assumption);
    try constructor.
  rewrite rev_involutive, app_nil_r in I; assumption.
Qed.

(** [zippy_decimal]: [zippy xs ys zs] means that [xs] represents
    the sum of the numbers represented by [ys] and [zs]. *)

Lemma zippy_decimal' (xs ys zs : list char)
  : zippy xs ys zs ->
    exists (m n p : nat),
      decimal_string' xs m /\
      decimal_string' ys n /\
      decimal_string' zs p /\
      m = n + p.
Proof.
  induction 1.
  - exists 0, 0, 0. repeat constructor.
  - destruct IHzippy as (m & n & p & X & Y & Z & E).
    exists (a + 10 * m), (b + 10 * n), (c + 10 * p).
    repeat constructor; auto.
    lia.
Qed.

Lemma zippy_append (xs1 xs2 ys1 ys2 zs1 zs2 : list char)
  : zippy xs1 ys1 zs1 ->
    zippy xs2 ys2 zs2 ->
    zippy (xs1 ++ xs2) (ys1 ++ ys2) (zs1 ++ zs2).
Proof.
  induction 1; cbn; intros; auto.
  - econstructor; eauto.
Qed.

Lemma zippy_rev (xs ys zs : list char)
  : zippy xs ys zs ->
    zippy (rev xs) (rev ys) (rev zs).
Proof.
  induction 1; cbn.
  - constructor.
  - apply zippy_append; auto.
    econstructor; eauto.
    constructor.
Qed.

Lemma zippy_decimal (xs ys zs : list char)
  : zippy xs ys zs ->
    exists (m n p : nat),
      decimal_string xs m /\
      decimal_string ys n /\
      decimal_string zs p /\
      m = n + p.
Proof.
  intros; apply zippy_decimal', zippy_rev; assumption.
Qed.

(** [solve_decimal]: One half of the final theorem. *)

Lemma solve_decimal
  : forall s n s1 s2,
      decimal_string s n ->
      (s1, s2) = solve s ->
      exists m1 m2,
        decimal_string s1 m1 /\
        decimal_string s2 m2 /\
        n = m1 + m2.
Proof.
  intros s n s1 s2 H I.
  apply solve_zippy in I; [ | eapply decimal_digits; eassumption].
  apply zippy_decimal in I.
  destruct I as (a & b & c & X & Y & Z & T).
  apply (decimal_string'_injective _ _ H) in X.
  subst a; eauto.
Qed.

(** [solve_no4]: The other half. The outputs of [solve] don't contain ["4"]. *)

Lemma _solve'_no4
  : forall s a1 a2 s1 s2,
      no4 a1 ->
      no4 a2 ->
      (s1, s2) = solve' s a1 a2 ->
      no4 s1 /\ no4 s2.
Proof.
  induction s; cbn; intros.
  - inversion H1; auto.
  - destruct (char_eqb a "4"%char) eqn:Ea.
    + eapply IHs;
        try apply H1;
        constructor; auto; apply ascii_char_neq; discriminate.
    + eapply IHs;
        try apply H1;
        constructor; auto.
      * apply char_neqb_neq; assumption.
      * apply ascii_char_neq; discriminate.
Qed.

Lemma solve_no4
  : forall s s1 s2,
      (s1, s2) = solve s ->
      no4 s1 /\ no4 s2.
Proof.
  intros s s1 s2; apply _solve'_no4; constructor.
Qed.

(** Final theorem *)
Theorem solve_correct : SOLVE_CORRECT.
Proof.
  red; intros s s1 s2 n H I.
  destruct (solve_no4 _ _ _ I) as (? & ?).
  destruct (solve_decimal _ _ _ _ H I) as (? & ? & ? & ? & ?).
  eauto 7.
Qed.
