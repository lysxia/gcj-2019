From Coq Require Import
  List
  NArith
  Lia.
From SimpleIO Require Import SimpleIO.

From Lib Require Import common.
From GCJ2019 Require Import qual_3.

Import ListNotations.

Local Open Scope N_scope.

Inductive prime (n : N) : Prop :=
| prime_def : n <> 1 -> (forall d, (d | n) -> d = 1 \/ d = n) -> prime n.

Fixpoint index (xs : list N) (i : N) : N :=
  match xs with
  | [] => 0
  | x :: xs =>
    if N.eqb 0 i then x else index xs (N.pred i)
  end.

Inductive sorted : list N -> Prop :=
| sorted_single x : sorted [x]
| sorted_two x y xs : x < y -> sorted (y :: xs) -> sorted (x :: y :: xs)
.

Definition encode (xs : list N) : list N :=
  zip_with N.mul xs (tl xs).

Definition CORRECT : Prop :=
  forall is ws,
    sorted ws ->
    (forall w, In w ws -> prime w) ->
    (forall i, i < N_of ws <-> In i is) ->
    let xs := map (index ws) is in
    let ys := encode xs in
    is = solve ys.

(**)

Lemma prime_spec (n : N)
  : prime n -> forall d, (d | n) -> d = 1 \/ d = n.
Proof. intros []; auto. Qed.

Lemma prime_nonzero (n : N)
  : prime n -> n <> 0.
Proof.
  intros [_ H] ?. subst n.
  destruct (H 2); try discriminate.
  exists 0; constructor.
Qed.

Lemma prime_nonone (n : N) : prime n -> n <> 1.
Proof. intros []; auto. Qed.

Hint Resolve prime_nonzero : core.

Lemma divide_mul_prime_nonzero (n p q : N)
  : p <> 0 ->
    q <> 0 ->
    (n | p * q) ->
    n <> 0.
Proof.
  intros Hp Hq Hn ?; subst n.
  apply N.divide_0_l, N.mul_eq_0_l in Hn; auto.
Qed.

Lemma prime_mul_not_prime (p q : N)
  : prime p -> prime q -> prime (p * q) -> False.
Proof.
  intros Hp Hq Hpq.
  destruct (prime_spec _ Hpq p).
  - apply N.divide_factor_l.
  - eapply (prime_nonone p); eauto.
  - apply (f_equal (fun x => x / p)) in H.
    rewrite N.div_same in H; eauto.
    rewrite N.mul_comm, N.div_mul in H; eauto.
    eapply (prime_nonone q); eauto.
Qed.

Lemma prime_gcd (p q r : N)
    (Pp : prime p) (Pq : prime q) (Pr : prime r)
  : p <> r -> N.gcd (p * q) (q * r) = q.
Proof.
  intros H.
  rewrite (N.mul_comm q).
  apply N.gcd_unique'.
  - apply N.divide_factor_r.
  - apply N.divide_factor_r.
  - intros m Hp Hr.
    apply N.divide_mul_split in Hp; [ | eauto using divide_mul_prime_nonzero ].
    apply N.divide_mul_split in Hr; [ | eauto using divide_mul_prime_nonzero ].
    destruct Hp as (mp & mq & Hm & Hmp & Hmq).
    destruct Hr as (mq' & mr & Hm' & Hmq' & Hmr).
    apply prime_spec in Hmp; auto.
    apply prime_spec in Hmq; auto.
    apply prime_spec in Hmq'; auto.
    apply prime_spec in Hmr; auto.
    destruct Hmp, Hmq, Hmq', Hmr; subst;
      try rewrite !N.mul_1_r in *; try rewrite !N.mul_1_l in *;
      try contradiction;
      try subst;
      try apply N.divide_refl;
      try apply N.divide_1_l.
    all: try match goal with
             | [ H : prime (_ _ _) |- _ ] => apply prime_mul_not_prime in H; eauto; contradiction
             end.
    all: try rewrite Hm';
      try apply N.divide_refl;
      try apply N.divide_1_l.
    apply (f_equal (fun x => x / q)) in Hm'.
    rewrite 2 N.div_mul in Hm'; auto; try contradiction.
Qed.

(**)

Inductive indexed : list N -> N -> N -> Prop :=
| index_here x xs : indexed (x :: xs) x 0
| index_there x y xs i : x <> y -> indexed xs x i -> indexed (y :: xs) x (N.succ i)
.

Lemma _translate_correct
  : forall is ws,
      sorted ws ->
      (forall i, i < N_of ws <-> In i is) ->
      let xs := map (index ws) is in
      sort xs = ws.
Proof.
  intros.
Admitted.

Lemma sorted_index
  : forall w ws,
      sorted (w :: ws) ->
      forall i, i < N.succ (N_of ws) -> w <= index (w :: ws) i.
Proof.
  intros w ws; revert w; induction ws; intros w Hws; inversion_clear Hws;
    intros i Hi.
  - apply N.lt_1_r in Hi; subst i. reflexivity.
  - specialize (IHws a H0 (N.pred i)).
    rewrite N_of_cons in Hi.
    cbn [ index ] in *. destruct (N.eqb_spec 0 i).
    { reflexivity. }
    destruct (N.eqb_spec 0 (N.pred i)).
    { apply N.lt_le_incl. auto. }
    eapply N.le_trans.
    { apply N.lt_le_incl; eauto. }
    apply IHws. apply N.succ_lt_mono. rewrite N.succ_pred by auto.
    auto.
Qed.

Lemma find_index
  : forall ws,
      sorted ws ->
      forall i, i < N_of ws -> find ws (index ws i) = i.
Proof.
  induction 1;
    cbn [ find index ] in *;
    intros i Hi;
    destruct (N.eqb_spec 0 i).
  - rewrite N.eqb_refl; auto.
  - cbn in Hi. apply N.lt_1_r in Hi. symmetry in Hi. contradiction.
  - rewrite N.eqb_refl; auto.
  - rewrite !N_of_cons in Hi. rewrite N_of_cons in IHsorted.
    rewrite <- (N.succ_pred i) in Hi by auto.
    apply N.succ_lt_mono in Hi.
    rewrite IHsorted; auto.
    replace (1 + N.pred i) with i by lia.
    rewrite (proj2 (N.eqb_neq x _)); [ reflexivity | ].
    apply N.lt_neq. eapply N.lt_le_trans; eauto using sorted_index.
Qed.

Lemma translate_correct
  : forall is ws,
      sorted ws ->
      (forall i, i < N_of ws <-> In i is) ->
      let xs := map (index ws) is in
      map (find (sort xs)) xs = is.
Proof.
  intros is ws Hws His xs; subst xs.
  rewrite _translate_correct; auto.
  rewrite map_map. rewrite (map_ext_in _ (fun x => x)).
  - rewrite map_id; reflexivity.
  - intros ? Hin; apply His in Hin.
    apply find_index; auto.
Qed.

Definition at_least_3 {A} (xs : list A) : Prop :=
  exists x y z,
    In x xs /\
    In y xs /\
    In z xs /\
    x <> y /\ y <> z /\ x <> z.

Lemma unprime_correct
  : forall ys,
    at_least_3 ys ->
    (forall y, In y ys -> prime y) ->
    unprime (encode ys) = ys.
Proof.
Admitted.

Theorem solve_correct : CORRECT.
Proof.
  red; intros. unfold solve.
  subst ys.
  rewrite unprime_correct.
  - subst xs; rewrite translate_correct; auto.
  - admit.
  - admit.
Abort.
