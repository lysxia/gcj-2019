From SimpleIO Require Import SimpleIO.
From GCJ2019 Require Import qual_1.

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "qual_1.ml" run_main.
