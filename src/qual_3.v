(* Cryptopangrams *)

From Coq Require Import
  ExtrOcamlIntConv
  NArith
  List
  Ascii
  String.
From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf Scanf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Local Open Scope N_scope.

Fixpoint deflate (p : N) (xs : list N) : list N :=
  p ::
    match xs with
    | [] => []
    | x :: xs => deflate (N.div x p) xs
    end.

Fixpoint _unprime (x : N) (xs : list N) (k : N -> list N -> list N) : list N :=
  match xs with
  | [] => []  (* Should not happen *)
  | x' :: xs =>
    let g := N.gcd x x' in
    if N.eqb x g then
      _unprime x' xs (fun p ys =>
        let q := N.div x' p in
        k q (q :: ys))
    else
      k g (g :: deflate (N.div x' g) xs)
  end.

Definition unprime (xs : list N) : list N :=
  match xs with
  | [] => []
  | x :: xs => _unprime x xs (fun p ys => N.div x p :: ys)
  end.

Fixpoint insert (n : N) (xs : list N) : list N :=
  match xs with
  | [] => [n]
  | x :: xs' =>
    match N.compare n x with
    | Lt => n :: xs
    | Eq => xs
    | Gt => x :: insert n xs'
    end
  end.

Fixpoint sort (xs : list N) : list N :=
  match xs with
  | [] => []
  | x :: xs => insert x (sort xs)
  end.

Fixpoint find (xs : list N) (n : N) : N :=
  match xs with
  | [] => 0
  | x :: xs =>
    if N.eqb x n then 0 else 1 + find xs n
  end.

Definition solve (xs : list N) : list N :=
  let primed := unprime xs in
  let f := sort primed in
  map (find f) primed.

(** * IO *)

Definition alpha (n : N) : char :=
  match char_of_int_opt (int_of_n (n + 65)) with
  | None => "_"%char
  | Some a => a
  end.

Definition main (_ : unit) : IO unit :=
  codejam_loop (fun (i : N) =>
    nn <- scanf "%Nd %Nd" (fun nN nL _ => Some (nN, nL)) ;;
    let '(nN, nL) := nn in
    xs <- read_Ns ;;
    print_string (sprintf "Case #%Nd: " i) ;;
    print_endline (OString.of_list (map alpha (solve xs)))).
