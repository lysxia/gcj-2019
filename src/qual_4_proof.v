From Coq Require Import
  Arith
  NArith
  List
  Lia.

From Lib Require Import common.
From GCJ2019 Require Import qual_4.

Import ListNotations.
Local Open Scope N_scope.

(** * Core definitions *)

(** ** Evaluator *)

(** The state of the world is described by a list of [bool], one for each worker,
    which is [true] if the worker is broken, [false] otherwise. *)

(** [eval xs ys]: Remove elements of [ys] corresponding to [true] in [xs].
    This corresponds to one round of testing. *)
Fixpoint eval {A} (xs : list bool) (ys : list A) : list A :=
  match xs, ys with
  | true :: xs, _ :: ys => eval xs ys
  | false :: xs, y :: ys => y :: eval xs ys
  | _, _ => []
  end.

(** [eval_tree xs t]: Execute the whole test tree, returning the final guess for
    the broken machines. *)
Fixpoint eval_tree (xs : list bool) (t : tree) : list N :=
  match t with
  | Out bs k => eval_tree xs (k (eval xs bs))
  | Guess r => r
  end.

(** While the state of the world is a [list bool], the output of the tree is in
    the form of a [list N], a list of (what we think are) the indices of the
    [true] elements in the initial list.

    The following function [indicator : list N -> list bool]
    allows us to compare the result [list N] to the actual solution [list bool]. *)

Definition _indicator (n : N) : list N -> N -> list bool :=
  N.iter n (fun go xs i =>
    match xs with
    | x :: xs' =>
      if N.eqb i x then
        true :: go xs' (N.succ i)
      else
        false :: go xs (N.succ i)
    | [] => false :: go [] (N.succ i)
    end) (fun _ _ => []).

Definition indicator (n : N) (xs : list N) : list bool :=
  _indicator n xs 0%N.

(** The number of rounds [nF] bounds the depth of the tree. This bound is defined as follows. *)

(** [depth_tree t n]: the tree [t] has depth at most [n].
    [Out] trees have depth at least one, and their children have depth one less.
    [Guess] trees have depth 0 (therefore it is at most [n] for any [n]). *)
Inductive depth_tree : tree -> N -> Prop :=
| depth_Out bs k n : (forall vs, depth_tree (k vs) n) -> depth_tree (Out bs k) (N.succ n)
| depth_Guess r n : depth_tree (Guess r) n
.

(** A small test just to make sure [depth_tree] isn't vacuously true. *)
Fact depth_tree_nontrivial : ~ depth_tree (Out [] (fun _ => Guess [])) 0.
Proof.
  intros H; inversion H. specialize (H1 []). inversion H1. subst. eapply N.neq_succ_0. eauto.
Qed.

(** * Main theorem *)

(** For any parameters [nN, nB, nF] and any hidden state of the world [xs],
    with some reasonable assumptions,
    the tree [tester nN nB nF] has depth at most [nF], and it recovers [xs]. *)
Definition CORRECT : Prop :=
  forall (nN nB nF : N) (xs : list bool),  (* Assumptions: *)
    0 < nF ->                              (* - At least one round *)
    nN = N_of xs ->                        (* - [nN]: The length of the list *)
    nB = B_of xs ->                        (* - [nB]: The number of failing bits *)
    (nB < 2 ^ nF)%N ->                     (* - Bound on the number of failing bits. *)
    depth_tree (tester nN nB nF) nF /\
    indicator nN (eval_tree xs (tester nN nB nF)) = xs.

(** Check that the bound holds for the constants given in the actual challenge. *)

Example test1_valid
  : let nB := 1023 in
    let nF := 10 in
    nB < 2 ^ nF.
Proof. constructor. Qed.

Example test2_valid
  : let nB := 15 in
    let nF := 5 in
    nB < 2 ^ nF.
Proof. constructor. Qed.

(** * General facts *)

(** ** Definition of [indices] *)

(** [indices : list bool -> list N] is the inverse of [indicator : list N -> list bool]:
    [indices] constructs a list of values from its indicator function.
    - [indicator] is the more natural way to state the correctness theorem
      (it explicitly describes how to reconstruct [xs]);
    - [indices] makes the proof easier. *)

Fixpoint _indices (i : N) (xs : list bool) : list N :=
  match xs with
  | [] => []
  | x :: xs => if x then i :: _indices (N.succ i) xs else _indices (N.succ i) xs
  end.

Definition indices : list bool -> list N := _indices 0.

(** Some facts about [indices] *)

Lemma indices_monotonic (i : N) (xs : list bool)
  : match _indices i xs with
    | [] => True
    | j :: _ => (i <= j)%N
    end.
Proof.
  revert i; induction xs; intros; cbn.
  - constructor.
  - destruct a. { reflexivity. } specialize (IHxs (N.succ i)). destruct _indices; auto.
    etransitivity; [ apply N.le_succ_diag_r | eassumption].
Qed.

Lemma indicator_indices (xs : list bool)
  : indicator (N_of xs) (indices xs) = xs.
Proof.
  enough (H :
    forall i,
      _indicator (N_of xs) (_indices i xs) i = xs).
  { apply H. }
  induction xs; intros.
  - reflexivity.
  - rewrite N_of_cons. cbn.
    unfold _indicator. rewrite N_iter_succ_l. fold (_indicator (N_of xs)).
    destruct a.
    + rewrite N.eqb_refl. f_equal. apply IHxs.
    + pose proof (IHxs (N.succ i)).
      pose proof (indices_monotonic (N.succ i) xs).
      destruct _indices eqn:Eindices.
      * f_equal. apply H.
      * assert ((i =? n)%N = false).
        { apply N.eqb_neq, N.lt_neq, N.le_succ_l. assumption. }
        rewrite H1.
        f_equal; assumption.
Qed.

(** This lemma applies to the conlusion of the main theorem. *)
Lemma indicator_indices' (nN : N) (xs : list bool) (ys : list N)
  : nN = N_of xs -> ys = indices xs -> indicator nN ys = xs.
Proof.
  intros; subst; apply indicator_indices.
Qed.

(** ** General facts about [eval] *)

(** The main fact is that it commutes with [transpose] ([transpose_eval]). *)

Lemma map_eval {A B} (f : A -> B) (xs : list bool) (ys : list A)
  : eval xs (map f ys) = map f (eval xs ys).
Proof.
  revert xs; induction ys as [ | y ys IHys ]; intros; cbn.
  - destruct xs as [ | [] ? ]; reflexivity.
  - destruct xs as [ | [] ? ]; cbn; try rewrite IHys; reflexivity.
Qed.

Lemma zip_with_eval {A B C} (f : A -> B -> C) (xs : list bool) (ys : list A) (zs : list B)
  : eval xs (zip_with f ys zs) = zip_with f (eval xs ys) (eval xs zs).
Proof.
  revert xs zs; induction ys as [ | y ys IHys ]; intros; cbn.
  - destruct xs as [ | [] ? ]; reflexivity.
  - destruct xs as [ | [] xs ]; cbn;
      [ reflexivity | | ];
      destruct zs;
      try rewrite IHys;
      try rewrite zip_with_nil_r;
      try reflexivity.
Qed.

Lemma transpose_eval {A} (xs : list bool) (ys : list (list A))
  : eval xs (transpose ys) = transpose (List.map (eval xs) ys).
Proof.
  revert xs; induction ys as [ | y ys IHys ]; intros; cbn.
  - destruct xs as [ | [] ? ]; cbn; reflexivity.
  - destruct ys eqn:Eys at 1.
    + subst ys. apply map_eval.
    + rewrite Eys at 2. cbn.
      rewrite <- IHys.
      apply zip_with_eval.
Qed.

(** ** General facts about binary representations *)

(** The most involved algorithm, in terms of proof effort, is the construction
    of a matrix of bits by counting in binary. We have to show that consecutive
    elements within a period are distinct, which is rather tricky to state correctly
    in the first place, but also in a way that is convenient to use. *)

(** An obvious approach is to relate those binary representations to natural numbers.
    But it would probably be quite cumbersome, heavy with modular arithmetic and
    list indexing.

    The main idea to keep things simple is that we can define the "difference"
    between binary representations directly, without explicitly viewing those
    binary representation as numbers themselves.

    The main result here is [succ_diff]: given any two endpoints [xs] and [ys],
    then the difference [diff xs ys] uniquely characterizes the number of times to
    count from [xs] to [ys] without exceeding a period ([2^F]). This allows us to
    establish that the difference is 0 if and only if [xs = ys]. *)

Lemma N_of_bin_succ xs : N_of (bin_succ xs) = N_of xs.
Proof.
  induction xs; [ reflexivity | ]; cbn [ bin_succ ].
  destruct a; rewrite !N_of_cons, ?IHxs; reflexivity.
Qed.

(** Difference between two binary numbers with a carry:
    [(y - x - c) mod (2 ^ F)] where [F] is the common length of [x] and [y]. *)
Fixpoint _diff (c : bool) (xs ys : list bool) : N :=
  match xs, ys with
  | [], [] => 0
  | x :: xs, y :: ys => N_bit (xorb c (xorb x y)) + N.double (_diff (if y then x && c else x || c) xs ys)
  | _, _ => 0%N (* Unused *)
  end.

Definition diff : list bool -> list bool -> N := _diff false.

Lemma N_iter_double_bin_succ (n : N) (b : bool) (xs : list bool)
  : N.iter (N.double n) bin_succ (b :: xs) = b :: N.iter n bin_succ xs.
Proof.
  revert xs; induction n using N.peano_ind; intros; [ reflexivity |].
  rewrite N_double_succ, !N_iter_succ_r; cbn.
  destruct b; cbn; rewrite IHn; reflexivity.
Qed.

Lemma _diff_succ (n : nat)
  : forall (c : bool) (xs ys : list bool),
    n = List.length xs ->
    n = List.length ys ->
    N.iter (_diff c xs ys) bin_succ (if c then bin_succ xs else xs) = ys.
Proof.
  induction n; destruct xs as [ | x xs ], ys as [ | y ys ]; try discriminate; cbn; intros.
  - destruct c; reflexivity.
  - apply Nat.succ_inj in H. apply Nat.succ_inj in H0.
    destruct x, y, c;
      try match goal with
      | [ |- _ (_ ?t _) _ _ _ = _ ] => change t with 0%N + change t with 1%N
      end;
      try rewrite N.add_1_l; try rewrite N.add_0_l;
      cbn;
      try rewrite N_iter_succ_r;
      try rewrite N_iter_double_bin_succ; cbn;
      try rewrite N_iter_double_bin_succ; cbn;
      try rewrite IHn; auto.
Qed.

Lemma diff_succ (xs ys : list bool)
  : N_of xs = N_of ys ->
    N.iter (diff xs ys) bin_succ xs = ys.
Proof.
  intros H. eapply _diff_succ. { reflexivity. } { rewrite 2 N_to_nat_length, H; reflexivity. }
Qed.

Inductive N_pow_case (e : N) : N -> Prop :=
| N_pow_case_even n : n < 2 ^ e -> N_pow_case e (N.double n)
| N_pow_case_odd n : n < 2 ^ e -> N_pow_case e (N.succ (N.double n))
.
Arguments N_pow_case_even {e}.
Arguments N_pow_case_odd {e}.

Lemma N_pow_split (e n : N)
  : n < 2 ^ N.succ e -> N_pow_case e n.
Proof.
  revert e; induction n using N.binary_ind; intros.
  - apply (N_pow_case_even 0). eapply N.lt_lt_0, N.pow_gt_lin_r. constructor.
  - constructor. rewrite N.pow_succ_r', N.double_spec in H.
    apply N.mul_lt_mono_pos_l in H; [ | constructor ].
    apply H.
  - rewrite N.succ_double_spec, <- N.double_spec, N.add_1_r. constructor.
    apply (N.mul_lt_mono_pos_l 2); [ constructor | ].
    rewrite N.pow_succ_r', N.succ_double_spec in H.
    transitivity (2 * n + 1); [ apply N.lt_add_pos_r; constructor | assumption ].
Qed.

Lemma _succ_diff (xs : list bool)
  : forall (c : bool) (n : N),
    n < 2 ^ N_of xs ->
    _diff c xs (N.iter (N_bit c + n) bin_succ xs) = n.
Proof.
  induction xs; [ | rewrite N_of_cons ]; intros c n H.
  - replace n with 0%N.
    + destruct c; reflexivity.
    + apply N.lt_1_r in H; auto.
  - destruct (N_pow_split _ _ H).
    + pose proof (IHxs false _ H0) as I.
      rewrite N_iter_add, N_iter_double_bin_succ.
      destruct a, c; cbn; f_equal; try apply I.
      rewrite <- N_iter_succ_l, <- N.add_1_l; apply (IHxs true _ H0).
    + pose proof (IHxs false _ H0) as I.
      rewrite N_iter_add, N_iter_succ_l, N_iter_double_bin_succ.
      destruct a, c; cbn - [ N.add ];
        rewrite <- N.add_1_l; f_equal; f_equal; auto;
        rewrite <- N_iter_succ_l, <- N.add_1_l; apply (IHxs true _ H0).
Qed.

Lemma succ_diff (n : N) (xs : list bool)
  : n < 2 ^ N_of xs ->
    diff xs (N.iter n bin_succ xs) = n.
Proof. apply _succ_diff. Qed.

Lemma self_diff xs : diff xs xs = 0.
Proof.
  unfold diff.
  induction xs; [ constructor | ].
  destruct a; cbn; rewrite IHxs; reflexivity.
Qed.

Lemma bin_succ_inj_n p xs
  : 1 + p < 2 ^ N_of xs ->
    N.iter (N.succ p) bin_succ xs <> xs.
Proof.
  intros H contra.
  pose proof (self_diff xs) as I.
  pose proof (succ_diff (N.succ p) xs) as J.
  rewrite contra in *.
  rewrite <- N.add_1_l in J at 1.
  rewrite (J H) in I.
  eapply N.neq_succ_0; eassumption.
Qed.

(** * Correctness proof *)

(** At a high level, the solution does the following:
    1. construct a matrix with certain properties;
    2. send that matrix, row by row, and interactively reconstruct a new matrix
       from the response, which should be obtained by removing some columns from
       the original one;
    3. compare the two matrices to find which columns were removed.
 *)

(** The proof structure roughly mirrors those three components.
    a. we formalize the property of the matrix that Step 3 relies on,
       and verify that the matrix defined in Step 1 has those properties
       (informally, that any consecutive chunk of columns are all distinct);
    b. we make sure that the matrix was correctly sent, so that if the
       environment behaves correctly, the constructed matrix is indeed
       described by the original matrix and the state of the world
       (the columns to remove);
    c. we verify that the final scan correctly recovers the removed columns. *)

(** Below, the proof follows order (b), (c), (a):
    b. [eval_tree_diagnostic]
    c. [analyze_correct]
    a. [test_matrix_distinctive] *)

(** The proof that the tree has depth at most [nF] is mostly independent from
    all that. *)

(** ** Correctness of interaction *)

(** (This is (b) above.) *)

(** [eval_tree_diagnostic]: The [diagnostic] function takes a matrix [m], sends
    it row by row, collects the corresponding responses into a new matrix which
    should be equal to [eval xs m], and runs [analyze] on it. *)

Lemma _eval_tree_diagnostic xs ys k
  : eval_tree xs (_diagnostic ys k) = k (map (eval xs) ys).
Proof.
  revert k; induction ys; intros; cbn; [ reflexivity |].
  rewrite IHys; reflexivity.
Qed.

Lemma eval_tree_diagnostic n m xs
  : Forall (fun r => N_of r = N.succ n) m ->
    eval_tree xs (diagnostic m) = analyze m (eval xs m).
Proof.
  intros H. unfold diagnostic.
  rewrite _eval_tree_diagnostic.
  rewrite <- transpose_eval.
  erewrite transpose_transpose; eauto.
Qed.

(** These lemmas verify that the constructed matrix has the expected dimensions. *)

Lemma N_of_test_matrix nN nF : N_of (test_matrix nN nF) = nN.
Proof. apply N_of_repeatf. Qed.

Lemma N_of_test_matrix_rows nN nF
  : Forall (fun r => N_of r = nF) (test_matrix nN nF).
Proof.
  apply Forall_repeatf.
  - intros. rewrite N_of_bin_succ. assumption.
  - apply N_of_repeat.
Qed.

Lemma N_of_transpose_test_matrix nN nF : 0 < nN -> N_of (transpose (test_matrix nN nF)) = nF.
Proof.
  intros H.
  assert (I : 0 < N_of (test_matrix nN nF)).
  { rewrite N_of_test_matrix; auto. }
  apply N.lt_exists_pred in I. destruct I as (? & I & _).
  apply positive_length in I. destruct I as (? & ? & I).
  rewrite I.
  apply N_of_transpose.
  rewrite <- I.
  apply N_of_test_matrix_rows.
Qed.

(** [eval_tree_tester]: [eval_tree_diagnostic] specialized to our [test_matrix]. *)

Lemma eval_tree_tester nN nB nF xs
  : 0 < nF ->
    eval_tree xs (tester nN nB nF) = analyze (test_matrix nN nF) (eval xs (test_matrix nN nF)).
Proof.
  intros H.
  apply N.lt_exists_pred in H. destruct H as (? & H & _). subst.
  eapply eval_tree_diagnostic, N_of_test_matrix_rows.
Qed.

(** * Error detection *)

(** The property [distinctive nMax] formalizes a sufficient condition on a list
    (a matrix is viewed as a special case, a list of columns) to be able to
    recover up to [nMax] removed elements. *)

Definition _distinctive {A} (nMax : N) (ys : list A) : Prop :=
  forall (y : A) (ys' : list A),
  ys = y :: ys' ->
  forall (x : bool) (xs' : list bool),
  N_of xs' = N_of ys' ->
  N_bit x + B_of xs' < nMax ->
  forall z zs,
  z :: zs = eval (x :: xs') (y :: ys') ->
  BoolSpec
    (z <> y /\ z :: zs = eval xs' ys')
    (z = y /\ zs = eval xs' ys')
    x.

Definition distinctive {A} (nMax : N) (ys : list A) : Prop :=
  Forall (_distinctive nMax) (tails ys).

(**)

Notation __analyze := (_analyze (list_eqb Bool.eqb)).

Inductive eval_case {A} : list bool -> list A -> list A -> Prop :=
| eval_case_nil : eval_case [] [] []
| eval_case_nil_cons xs y ys : eval_case xs ys [] -> eval_case (true :: xs) (y :: ys) []
| eval_case_cons_eq xs y ys zs
  : eval_case xs ys zs -> eval_case (false :: xs) (y :: ys) (y :: zs)
| eval_case_cons_neq xs y ys z zs
  : y <> z -> eval_case xs ys (z :: zs) -> eval_case (true :: xs) (y :: ys) (z :: zs)
.

Lemma eval_spec {A} (nMax : N) (xs : list bool) (ys : list A)
  : distinctive nMax ys ->
    List.length xs = List.length ys ->
    B_of xs < nMax ->
    eval_case xs ys (eval xs ys).
Proof.
  intros H; revert xs; induction ys as [ | y ys IHys ]; intros [ | x xs ] I J;
    try rewrite B_of_cons in J;
    try discriminate.
  - constructor.
  - red in H. rewrite unfold_tails in H; apply Forall_inv_cons in H; destruct H.
    red in H. specialize (H _ _ eq_refl x xs).
    cbn in I; apply Nat.succ_inj in I.
    assert (KN : N_of xs = N_of ys).
    { rewrite 2 N_of_nat_length; auto. }
    specialize (H KN J).
    assert (KB : B_of xs < nMax).
    { eapply N.le_lt_trans; [ | eassumption ]. rewrite N.add_comm; apply N.le_add_r. }
    specialize (IHys H0 _ I KB).
    destruct x; cbn; cbn in H.
    + destruct IHys; repeat constructor; eauto;
        specialize (H _ _ eq_refl);
        inversion H.
      destruct H1; auto.
      destruct H2; auto.
    + constructor. auto.
Qed.

(** [analyze_correct]: Given a [distinctive] matrix, the [analyze] function
    correctly recovers the removed columnes. *)

Lemma _analyze_correct (nMax : N) (xs : list bool) (ys : list (list bool))
    (HD : distinctive nMax ys)
    (HL : List.length xs = List.length ys)
    (HB : B_of xs < nMax)
  : forall i,
    __analyze i ys (eval xs ys) = _indices i xs.
Proof.
  pose proof (eval_spec nMax xs ys HD HL HB) as I.
  revert I; clear. intros I; induction I; cbn; intros.
  - reflexivity.
  - rewrite IHI; reflexivity.
  - destruct (bool_list_eqb_spec y y); [ auto | contradiction ].
  - destruct (bool_list_eqb_spec y z); [ contradiction | rewrite IHI; reflexivity ].
Qed.

Lemma analyze_correct nMax ys xs
  : distinctive nMax ys ->
    N_of xs = N_of ys ->
    B_of xs < nMax ->
    analyze ys (eval xs ys) = indices xs.
Proof.
  intros; eapply _analyze_correct; eauto.
  apply N_eq_length; assumption.
Qed.

(** [test_matrix_distinctive]: The [test_matrix] is [distinctive]. *)

Lemma test_matrix_distinctive nN nF
  : distinctive (2 ^ nF) (test_matrix nN nF).
Proof.
  apply (Forall_tails_repeatf (fun y => N_of y = nF)).
  1:{ intros; subst; apply N_of_bin_succ. }
  2:{ apply N_of_repeat. }
  red. intros n y0 [] y ys'.
  destruct (N.zero_or_succ n) as [ | [ m ? ] ]; subst n.
  { discriminate. }
  rewrite repeatf_succ.
  intros E; inversion_clear E; clear nF y0.
  rewrite N_of_repeatf.
  intros x xs' HNx HBx.
  destruct x; cbn.
  2:{ intros z zs E; inversion_clear E. auto. }
  enough (I :
    forall (xs1 : list bool) (m' p : N),
      N_of xs1 = m' ->
      1 + p + B_of xs1 < 2 ^ N_of y ->
      forall z zs,
      z :: zs = eval xs1 (N_repeatf bin_succ (N.iter p bin_succ (bin_succ y)) m') ->
      z <> y).
  { constructor; split; auto. eapply I with (p := 0); eauto. }
  clear. intros xs; induction xs as [ | x xs IHxs ]; [ discriminate |].
  rewrite N_of_cons, B_of_cons. cbn - [ N.add ]. intros m' p HN HB z zs.
  subst m'. rewrite repeatf_succ, <- N_iter_succ_l.
  destruct x; cbn [ N_bit ] in HB.
  - apply IHxs; auto. lia.
  - rewrite <- N_iter_succ_r.
    intros E; inversion_clear E.
    apply bin_succ_inj_n.
    lia.
Qed.

(** [tester_correct_depth]: The tree has depth (at most) [nF]. *)

Lemma _diagnostic_depth (zs : list (list bool))
  : forall k, depth_tree (_diagnostic zs k) (N_of zs).
Proof.
  induction zs; try rewrite N_of_cons; constructor; auto.
Qed.

Lemma diagnostic_depth (ys : list (list bool)) (nF : N)
  : N_of (transpose ys) = nF ->
    depth_tree (diagnostic ys) nF.
Proof. intros []; apply _diagnostic_depth. Qed.

Lemma tester_correct_depth nN nB nF
  : depth_tree (tester nN nB nF) nF.
Proof.
  destruct (N.eq_0_gt_0_cases nN).
  - subst; cbn; constructor.
  - apply diagnostic_depth, N_of_transpose_test_matrix. auto.
Qed.

(** Put everything together. *)

(** * Final theorem *)

Theorem tester_correct : CORRECT.
Proof.
  red; intros; split.
  - apply tester_correct_depth.
  - apply indicator_indices'; auto.
    rewrite eval_tree_tester by assumption.
    apply (analyze_correct (2 ^ nF)).
    + apply test_matrix_distinctive.
    + rewrite N_of_test_matrix. auto.
    + subst nB. assumption.
Qed.
