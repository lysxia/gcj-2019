From Coq Require Import
  ExtrOcamlIntConv
  NArith
  List
  Ascii
  String.

From SimpleIO Require Import SimpleIO.
From Printf Require Import Printf.

From Lib Require Import common.

Import IO.Notations.
Import StringSyntax.
Import ListNotations.

Definition solution (xs : list bool) : string :=
  let append_word s s' :=
    match s' with
    | ""%string => s
    | _ => (s ++ " " ++ s')%string
    end in
  List.fold_right (fun (x : bool) go i =>
    let s := go (N.succ i) in
    if x then
      append_word (decimal_string i) s
    else
      go (N.succ i)) (fun _ => ""%string) xs 0%N.

Definition to_line : list bool -> list char :=
  map (fun b : bool => if b then char_of_ascii "1"%char else char_of_ascii "0"%char).

Definition from_line : list char -> list bool :=
  List.map (fun x => char_eqb "1"%char x).

Fixpoint eval {A} (xs : list bool) (ys : list A) : list A :=
  match xs, ys with
  | true :: xs, _ :: ys => eval xs ys
  | false :: xs, y :: ys => y :: eval xs ys
  | _, _ => []
  end.

Definition read_line_ : IO ocaml_string :=
  s <- read_line ;;
  prerr_string " > "%string ;;
  prerr_endline s ;;
  IO.ret s.

Definition print_endline_ (s : ocaml_string) : IO unit :=
  print_endline s ;;
  prerr_string "<  "%string ;;
  prerr_endline s.

(* N.B.: There is a corner case where the solution can be undistinguishable
   from a query if it's just one number (B = N-1). Just don't do that. *)
Definition run (nF : N) (xs : list bool) : IO unit :=
  let sol := to_ostring (solution xs) in
  print_endline_ (sprintf "%Nd %Nd %Nd" (N_of xs) (B_of xs) nF) ;;
  IO.fix_io (fun self (_ : unit) =>
    s <- read_line_ ;;
    if ostring_eqb sol s then
      print_endline_ "1"%string
    else
      let r := from_line (OString.to_list s) in
      print_endline_ (OString.of_list (to_line (eval xs r))) ;;
      self tt
    ) tt.

Definition main (_ : unit) : IO unit :=
  print_endline_ "2"%string ;;
  run 3 [false; true; true; true; false] ;;
  run 4 [false; true; false; true; false; false; false; true; false].

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "qual_4_test.ml" run_main.
