From SimpleIO Require Import SimpleIO.
From GCJ2019 Require Import qual_4.

Definition run_main : io_unit := IO.unsafe_run (main tt).
Extraction "qual_4.ml" run_main.
