.PRECIOUS: build/%.ml src/%.vo build/%.native
.PHONY: %.run

qual_3: qual_3.run

qual_1: qual_1.run

qual_2: qual_2.run

qual_4: fifo1 build/qual_4.native build/qual_4_test.native
	./build/qual_4.native < fifo1 > fifo2 &
	./build/qual_4_test.native > fifo1 < fifo2


%.run: build/%.native
	$< < test/$*.input | tee test/$*.output
	if [ -e test/$*.ref ] ; then diff test/$*.ref test/$*.output ; fi

build/:
	mkdir -p build/

fifo1 fifo2:
	mkfifo fifo1 fifo2

COQPREFIX := GCJ2019

build/%.ml: build/ lib/common.vo src/%.vo src/%_extract.v
	cd build/ && coqc -Q ../lib Lib -Q ../src $(COQPREFIX) ../src/$*_extract.v
	rm build/$*.mli

build/%_test.native: build/ build/%.native
	cd build/ && coqc -Q ../lib Lib -Q ../src $(COQPREFIX) ../src/$*_test.v
	cd build/ && ocamlbuild $*_test.native

src/%.vo: lib/common.vo src/%.v src/%_proof.v
	coqc -Q lib/ Lib -Q src/ $(COQPREFIX) src/$*.v
	coqc -Q lib/ Lib -Q src/ $(COQPREFIX) src/$*_proof.v

build/%.native: build/%.ml
	cd build/ && ocamlbuild $*.native

lib/common.vo: lib/common.v
	coqc -Q lib/ Lib lib/common.v

clean:
	rm -rf build/
	rm -f fifo1 fifo2 src/*.{vo,glob} src/.*.aux lib/*.vo .nia.cache .lia.cache
